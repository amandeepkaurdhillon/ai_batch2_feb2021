marks = [10,33,100,20,90,70, 81]

#Filter function takes a function and sequence as arguments

def passing_marks(x):
    if x>=33:
        return True 
    else:
        return False

pass_students = list(filter(passing_marks, marks))
print(pass_students)

m = list(filter(lambda a:a>=33, marks))
print("m=",m)