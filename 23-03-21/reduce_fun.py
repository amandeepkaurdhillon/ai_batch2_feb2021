from functools import reduce

# data = [90,89,12,34,110,120,23,77]
data = [10,20,30,40]

# result = reduce(lambda x,y:x+y, data)
result = reduce(lambda x,y:x*y, data)
print(result)