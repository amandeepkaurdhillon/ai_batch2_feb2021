total_marks = 120
data = [90,89,12,34,110,120,23,77]
perc = []

for i in data:
    p = round((i/total_marks)*100,2)
    perc.append(p)
print(perc)

# Map Function: To apply specific operation on list of elements

result = list(map(lambda x:round((x/total_marks)*100,2), data))
print("result=", result)

names = ["petER","jAmes","jacK","joHnsan"]
rs = list(map(lambda a:a.upper(),names))
print(rs)