def add(x,y):
    return x+y

print(add(10,20))

a = lambda c,d:c+d 
print(a(20,30))


# xy = lambda x:x%2==0
# xy = lambda x:"Even" if x%2==0 else "Odd"
xy = lambda x:"Greater" if x>10 else("Equal" if x==10 else "Lesser")
print(xy(int(input("Enter Number to check: "))))