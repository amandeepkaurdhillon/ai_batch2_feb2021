import os  
options = '''
********************* OPERATIONS *********************************

        Press 1 :- to Add new Book.
        Press 2 :- to View all Books.
        Press 3 :- to add Content in a Particular Book.
        Press 4 :- to view Content of Particular Book.
        Press 5 :- to Delete content of a Particular Book.
        Press 6 :- to Delete Book.
        Press 7 :- to exit.

'''
def addFile():
    bookName        = input('Enter Book name. ')
    status_ = checkFileExists(bookName)
    # print(status_)
    if status_ == 1:
        file = open('{}.py'.format(bookName),'w')
        return '''
                Book Added successfully in your Library.
                '''
    return status_

def viewAllBook():
    dir_ = os.listdir()
    print('''
            *** In your Library these are the books ***  
          ''')
    dir_.remove(os.path.basename(__file__))
    for i in range(len(dir_)):
        print('Book Name = {}'.format(dir_[i]))

def addContent():
    bookName = input('Enter Book name. ')
    dir_ = os.listdir()
    if '{}.py'.format(bookName) in dir_:
        print('''
                *** Yeah! Book found in the library ***
              ''')
        content = input('Enter Content to add in the Book. ')
        file = open('{}.py'.format(bookName),'w')
        file.write(content)
        return '''
                Content Added successfully in the Book
               '''
    return '''
              Opps! Book not Found
           '''

def viewBookContent():
    bookName = input('Enter Book name. ')
    dir_ = os.listdir()
    if '{}.py'.format(bookName) in dir_:
        print('*** Yeah! Book found in the library ***')
        file = open('{}.py'.format(bookName),'r')
        print('''
                Content in the Book
               ''')
        print(file.read())
        return 0
    print('''
              Opps! Book not Found
           ''')
    return 

def deleteContent():
    bookName = input('Enter Book name. ')
    dir_ = os.listdir()
    if '{}.py'.format(bookName) in dir_:
        print('*** Book found in the library ***')
        confirm = input('''Are You sure, You want to delete Content.  
                        Press 1 To delete Content
                        Press 2 To exit this option
                        ''')
        if confirm == '1':
            file = open('{}.py'.format(bookName),'w')
            file.write('')
            return 'Book Content Delete.'
        else:
            return ''
    return '''
              Opps! Book not Found
           '''

def deletBook():
    bookName = input('Enter Book name. ')
    dir_ = os.listdir()
    if '{}.py'.format(bookName) in dir_:
        print('*** Book found in the library ***')
        confirm = input('''Are You sure, You want to delete Book. 
                        Press 1 To delete
                        Press 2 To exit this option
                        ''')
        if confirm == '1':
            os.remove('{}.py'.format(bookName))
            return 'Book Deleted successfully.'
        else:
            return ''
    return '''
              Opps! Book not Found
           '''

def checkFileExists(fileName):
    dir_ = os.listdir()
    if '{}.py'.format(fileName) in dir_:
        return '''
                Sorry! Book is already in the Library
               '''
    else:
        return 1


print(options)
rec = []
while True:
    choice_ = input('Enter Option. ')
    if choice_ == '1':
        print(addFile())
    elif choice_ == '2':
        viewAllBook()
    elif choice_ == '3':
        print(addContent())
    elif choice_ == '4':
        viewBookContent()
    elif choice_ == '5':
        print(deleteContent())
    elif choice_ == '6':
        print(deletBook())
    elif choice_ == '7':
        print('''
                :) Have a Nice day! Goodbye
              ''')
        break
    else:
        print('''
                :( Oops!
                Invalid option. Try Again
                ''')        