import re 

st = "aman hello there cat rat hello sat cmat helmat"

# rs = re.findall("hello",st)
# rs = re.findall("[a-z]+at",st)
# rs = re.findall("[A-Z]*at",st)
# rs = re.findall("[A-Z]*at",st, re.IGNORECASE)

# rs = re.search(r"^h",st)
rs = re.search(r"helmat$",st)

if rs==None:
    print("It does not end with 't'")
else:
    print("It ends with 't'")

print(re.search(r"\s$", st))

print(re.findall("[a-m]+at", st))