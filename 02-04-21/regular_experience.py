import re

mystr = "aman054_ %^$! python easy 678 is easy 1234 language"

# r = re.search("easy",mystr)
# r = re.search("\d",mystr)
# r = re.search("\d+",mystr)
# r = re.search("\s",mystr)
# r = re.search("\w+",mystr)
# r = re.search("\W+",mystr)
r = re.search("\S+",mystr)

print(r)
print("Span = ", r.span())
print("String = ", r.string)
print("Group = ", r.group())

# Note: \d \s are called special sequences
# \w word characters = A-Za-z0-9_
# span() returns thye location(indexes) of searched item
# string returns the string in which search took place
# group() returns the searched string, where there eas match