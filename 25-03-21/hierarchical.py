class Register:
    branch = "CSE"
    def __init__(self, name, title):
        self.name = name
        self.title = title
'''HIERARCHICAL INHERITANCE'''
class Account(Register):
    def fee_details(self,total,pending):
        print("Here are fee details of {} {} ({})".format(self.title,
        self.name,self.branch))
        print("Total Amount: Rs.{}/-".format(total))
        print("Pending Amount: Rs.{}/-".format(pending))
        print("Paid Amount: Rs.{}/-\n".format(total-pending))

class Library(Register):
    def book_details(self,bname,isdt):
        print("Books issued by: {} {} ({})".format(self.title,
        self.name, self.branch))
        print("BOOK NAME: {}\nISSUED ON:{}\n".format(bname,isdt))

s1 = Account("James","Er.")
s1.fee_details(10000,9000)

s2=Account("Candas","Miss")
s2.fee_details(10000,987)

lb = Library("Perry","Mr.")
lb.book_details("Computer Graphics","10-03-2021")