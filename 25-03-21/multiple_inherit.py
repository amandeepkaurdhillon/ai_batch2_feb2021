class Library:
    x = 10
    def lib(self):
        print("I am a function of library class")

class Account:
    y = "Account section"
    def intro(self):
        print("Students can query fee related questions here!")

#Multiple inheritance
class Student(Account,Library):
    clz = "ABCD Engg. College"
    def myfunc(self):
        print(" I am myfunc inside student class")

s1 = Student()
print(s1.clz)
s1.myfunc()
s1.intro()
s1.lib()
print(s1.y)
s2=Account()
print(issubclass(Account, Library))
print(issubclass(Student, Library))
