class Student:
    def greet(self):
        print("Have a wonderfull day!")
    def register(self,name):
        self.name = name
        print("{} registered successfully!".format(name))

'''Single Inheritance: Inherit Library class from Student Class'''
class Library(Student):
    def hello(self):
        print("Hii everyone!")
    def issue_book(self,bname, isdt, rtdt):
        print("{} has issued following book".format(self.name))
        print("Book Name: {}".format(bname))
        print("Issue date: {}".format(isdt))
        print("Return date: {}\n".format(rtdt))

ob = Library()
ob.hello()
ob.register("James")
ob.issue_book("Python programming","25-03-2021","10-04-2021")

ob1 = Student()
ob1.greet()
# ob1.hello() ## Parent class can't use child class's attributes