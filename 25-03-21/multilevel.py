class GrandParent:
    a = "Hello There"
    def fun1(self):
        print("Fun of Grand Parent")

class Parent(GrandParent):
    b = "data member"
    def fun2(self):
        print("Function of parent class")

class Child(Parent):
    c = "child class"
    def fun3(self):
        print("Function of child class")

obj = Child()
print(obj.c)
print(obj.b)
print(obj.a)

obj1 = Parent()
# print(obj1.c) ## Parent can't access child's attributes
print(obj1.b)
print(obj1.a)

