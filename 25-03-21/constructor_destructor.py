class Student:
    def __init__(self, name, rno, age):
        self.n = name
        self.a = age
        self.r = rno
        # print("{} registered successfully!".format(name))
        # print("I Will call automatically")

    def intro(self):
        print("{} is {} years old, roll number={}".format(self.n,self.a,self.r))
    
    def __del__(self):
        del self.n
        print("deleted successfully!")
        # print(self.n)

s1 = Student("Peter",1,18)
# s2 = Student("Jack",2,19)
# s3 = Student("Ammy",4,17)
# s4 = Student("Johnson",5,21)
s1.intro()
# s3.intro()