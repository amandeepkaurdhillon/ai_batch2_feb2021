import requests

city_name = input("Enter City Name: ")
url = "http://api.openweathermap.org/data/2.5/forecast?q={}&appid=185e8105f692c832734e4ea3b68ba805".format(city_name)

data = requests.get(url).json() 
date = input("Enter Date: ")
for i in data["list"]:
    if i["dt_txt"].split()[0]==date:
        print("Here are weather deatails of {} ({})".format(data["city"]["name"],data["city"]["country"]))
        print("\t",i["dt_txt"])
        print("\tTemperature: {}°C".format(i["main"]["temp"]))
        print("\tHumidity: {}°C".format(i["main"]["humidity"]))
        print("\t{} ({})".format(i["weather"][0]["main"],i["weather"][0]["description"]))
        print("================================")