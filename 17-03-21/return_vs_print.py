def cube(a):
    # print(a**3)
    return a**3

# print(cube(2))
# print(cube(2)+10)

# def myfun():
#     x = 30
#     return x
# print(myfun())

def myfun(a,b):
    c = a+b
    print("Sum=",c)
    return c
    # These statements will not execute because these are after return
    print("End of function")
    print("End of function")
    print("End of function")

myfun(10,20)
print("Outside the function")