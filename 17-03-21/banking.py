import random
options = '''
    Press 1: To create account
    Press 2: To check balance
    Press 3: To deposit
    Press 4: To withdraw
    Press 5: To exit
'''
print(options)
users = []
def create_account():
    name = input("Please Enter Your Name: ")
    bal = float(input("Enter Initial Amount: "))
    usr = {"name":name,"balance":bal,"pin":str(random.randint(1000,9999))}
    users.append(usr)
    msz = "Dear {} Your acount created successfully, Your PIN: {}\n"
    print(msz.format(name,usr["pin"]))

def check_balance():
    pin=input("Enter Your Pin: ")
    match = False 
    for user in users:
        if user["pin"]==pin:
            match = True 
            print("Welcome {}!!!".format(user["name"]))
            print("Here is your balance: Rs.{}/- \n".format(user["balance"]))
    if match==False:
        print("You are not registered with us!!! \n")

def deposit():
    pin=input("Enter Your Pin: ")
    match = False 
    for user in users:
        if user["pin"]==pin:
            match = True 
            print("Welcome {}!!!".format(user["name"]))
            amt = float(input("Enter Amount to deposit: "))
            user["balance"] = user["balance"]+amt 
            msz = "Your account credited with Rs.{}/- Current balance: Rs.{}/- \n"
            print(msz.format(amt,user["balance"]))

    if match==False:
        print("You are not registered with us!!! \n")

def withdraw():
    pin=input("Enter Your Pin: ")
    match = False 
    for user in users:
        if user["pin"]==pin:
            match = True 
            print("Welcome {}!!!".format(user["name"]))
            amt = float(input("Enter Amount to withdraw: "))
            if amt <= user["balance"]: 
                user["balance"] = user["balance"]-amt 
                msz = "Your account debited with Rs.{}/- Current balance: Rs.{}/- \n"
                print(msz.format(amt,user["balance"]))
            else:
                print("You don't have sufficiet balance to make this transaction! \n")

    if match==False:
        print("You are not registered with us!!! \n")

while True:
    choice = input("Please Enter Your Choice: ")
    if choice=="1":
        create_account()
    elif choice=="2":
        check_balance()
    elif choice=="3":
        deposit()
    elif choice=="4":
        withdraw()
    elif choice=="5":
        break
    else:
        print("Invalid Choice!")