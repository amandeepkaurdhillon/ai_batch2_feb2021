def profile(name,age,salary):
    print("Name: ",name)
    print("Age: ",age)
    print("Salary: ",salary,"\n")

## Positional Arguments
profile("Peter",22,1000)
profile(21,1000,"James")
# profile(21,1000,1,1) ## Error

## Keyword Argumets
profile(age=21,salary=1000,name="James")
# profile(age=21,salary=1000) ## Error name missing
# profile(age=23,"harry",20000) ## Error
profile("Aman", salary=2000, age=20)
profile("Schinchan", 5, salary=5)


