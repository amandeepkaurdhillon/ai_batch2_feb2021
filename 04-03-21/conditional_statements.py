x = 21
y = 20
## If else
if x>y:
    print("x is greater\n")
else:
    print("y is greater\n")

# if elif else ("Multiple conditions")
u = ["Aman","Peter","Jack"]
username = input("Enter Username: ")
if username=="":
    print("Username is required!!")
else:
    if username=="admin":
        print("Welcome Admin, How are you?\n")
    elif username in u:
        print("Hii {} Thanks for joining with us!\n".format(username))
    else:
        print("Sorry, We could not find you! \n")

# =
# ==