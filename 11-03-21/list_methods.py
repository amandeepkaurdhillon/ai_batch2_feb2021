x = ["Red","Green","Blue","Orange","Magenta"]
# print(dir(x))
print("Before: ", x)

## Insert Element
# x.insert(0,"yellow")
# x.insert(3,["yellow","gray"])
# print(x[3][1])
# x.append("Yellow")
# x[5]="Gray"

## Update Element
# x[0]="Pink"
# x[-2]="Cyan"

## Delete Element
# x.remove("Green")
# x.pop()
# print(x.pop(),"removed successfully!")
# print(x.pop(3),"removed successfully!")
# del x[1]
# del x ## Delete list
y = x.copy()
x.clear()
print("After: x=",x)
print("After: y=",y)

a = [10,20,40,20,10,30,4,6,20]
# print(a.index(20,2))
# print(a.index(20,4))
# print(a.index(20,11)) ## Will return error as 11 is not in list


print(a.count(40))
print(a.count(20))
print(a.count(11))

a.sort()
print(a)
a.reverse()
print(a)
y.reverse()
y.sort()
print(y)

e = [10,20]
f = [40,54]

# e.append(f)
e.extend(f)
print("extend=",e)

