c = """
    Press 1: To add number
    Press 2: To view 
    Press 3: To sum
    Press 4: To remove number
    Press 0: To exit
"""
print(c)
all = []
while True:
    ch = input("Enter Your Choice: ")
    if ch=="1":
        num = int(input("Enter number to add: "))
        all.append(num)
        print("Number {} added successfully \n".format(num))
    elif ch=="2":
        print("Total numbers: {}, {}\n".format(len(all), all))
    elif ch=="3":
        # print(sum(all)) 
        s = 0
        m = ""
        for i in all:
            s  = s+i
            m += str(i)+"+"
            
        print(m[:-1]+"="+str(s))
    elif ch=="4":
        num_to_remove = int(input("Enter number to remove: "))
        if num_to_remove not in all:
            print("Number {} not exist in list\n".format(num_to_remove))
        else:
            all.remove(num_to_remove)
            print("{} removed successfully! \n".format(num_to_remove))
    elif ch=="0":
        break
    else:
        print("Invalid Choice")