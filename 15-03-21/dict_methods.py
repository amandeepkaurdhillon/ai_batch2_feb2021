employee = {
    "name":"Harry Nohara",
    "emp_code":1001,
    "salary":10000.50,
    "specialization":["Coding","Test"],
    "is_permanent":True,
    "technology":{
        "front-end":["HTML","CSS","Tkinter"],
        "back-end":["Python","Django","SQL"],
    }
}

# print(employee["technology"]["front-end"][2])
# print(dir({}))

# print(len(employee.keys()))
# for i in employee.keys():
#     print(i)

# print(employee.values())
# print(employee.items())
# for item in employee.items():
#     print(item)

# for key,value in employee.items():
#     print(key,"=",value)

## INSERT, UPDATE, DELETE
# employee["age"]=22

# employee.update({"name":"Peter Parker","age":22,"location":"Mohali, Punjab"})

food = {"item_name":"Burger","price":40.0,"delivery_time":"30 Minutes","type":"Veg"}
# print("Before: ", food)

# food.pop("delivery_time")
x = food.popitem()
# print("After: ", food)

# print("x=",x)
# copy_food = food.copy()
# food.clear()
# print("copy_food=",copy_food)
# print("food=",food)

val = ["Student Name","Roll No.","Age","Email","Phone"]
new = {}.fromkeys(val,"N/A")
new.update({"Student Name":"James","Age":28})
print(new)
