def add(x,y):
    print(x+y)

add(10,20)
# add(10) # Error, this function takes two arguments


def names(*n):
    print("Total: ", len(n))
    print(n)

names()
names("Aman")
names("Aman","Kiran")
names("Aman","Kiran","James","Harry")

def multiply(*args):
    m=1
    for i in args:
        m = m*i 
    print("Result: ", m)

multiply(10,20,34,5)
multiply(10,20)