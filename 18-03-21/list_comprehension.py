ls = []
for i in range(10):
    ls.append(i)
# print(ls)

# LIST COMPREHENSION LIMITS THE EFFORTS
# a = [i for i in range(10)]
a = ["Hello" for i in range(10)]
print(a)


## Condition inside list comprehension

eight = []
for i in range(1,101):
    if i%8==0:
        eight.append(i)
# print(eight)

xy = [i for i in range(1,101) if i%8==0]
print(xy)
