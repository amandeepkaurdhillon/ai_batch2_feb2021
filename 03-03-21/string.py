info = "Python is created by Guido Van Rossum"
xy = "HELLO WORLD"
print(info)

# INDEXING
print(info[0])
print(info[-1])

print(len(xy))

#Slicing
print(xy[1:])
print(xy[:2])
print(xy[2:7])
print(xy[-3:])