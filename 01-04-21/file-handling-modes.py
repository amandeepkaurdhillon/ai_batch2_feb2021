f = open('abcd.txt','a')

print("Location: ", f.tell())
a = ""
n = 3
for i in range(10):
    a += "{} x {} = {} \n".format(n,i,n*i)
f.write(a)
print("Location: ", f.tell())
f.close()
