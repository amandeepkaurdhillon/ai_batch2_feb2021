import requests
data = requests.get("https://restcountries.eu/rest/v2/all").json()
name = input("Enter Country Name: ")
rs = ""
m = False
for i in data:
    if i["name"].lower()==name.lower():
        m = True
        rs += "<div style='padding:20px;margin:auto;width:35%;box-shadow:0px 0px 10px gray;'>"
        rs += "<h1 style='text-align:center'>{}</h1>".format(i["name"].upper())
        rs += "<ol type='1'>"
        for border in i["borders"]:
            rs += "<li>{}</li>".format(border)
        rs+= "</ol>"
        rs += "<img src='{}' style='height:200px;width:100%'/>".format(i["flag"])
        lg ="<h2 style='color:red'> Languages: "
        for lang in  i["languages"]:
            lg += lang["name"]+","
        rs += "{}</h2>".format(lg)
        rs += "</div>"
if m == True:
    path = "countries/{}.html".format(name.lower())
    f = open(path,'w')
    f.write(rs)
    f.close()
    print("File {} created successfully!".format(path))
else:
    print("Sorry '{}' could not found!".format(name))