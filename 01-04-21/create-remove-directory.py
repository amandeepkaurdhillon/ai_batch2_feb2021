import os

# # To create a folder
# os.mkdir("test")
# print("Folder created successfully!")

# # To remove folder
# os.rmdir("test")
# print("Folder removed successfully!")


# To check files/folders in give directory
# print(os.listdir())

# print("Countries folder = ",os.listdir("countries"))

if "test" in os.listdir():
    os.rmdir("test")
    print("Folder removed successfully!")
else:
    os.mkdir("test")
    print("Folder created successfully!")