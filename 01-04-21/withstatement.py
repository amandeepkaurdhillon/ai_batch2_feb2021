# f = open("abcd.txt","r+")
# data = f.read()
# print(data)
# f.close()
# f.write("hello there")
# print("written successfully!")

with open("abcd.txt","r+") as f:
    print(f.read())
    f.write("How are you?")
    print("written successfully! ")
    

# print(f.readlines()) ##with statement closes a file automatically