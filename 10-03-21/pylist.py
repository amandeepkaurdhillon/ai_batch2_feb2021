x = ["Python","PHP","JS","Bootstrap","JQuery"]

print(x)
print(len(x))

## Indexing
print(x[1])
print(x[-1])

## Slicing
print(x[1:])
print(x[:1])
print(x[-2:])
print(x[:-2])

print(x[:-1]+x[2:])

## Step
# print(x[::2])
# print(x[::-2])
# print(x[::-1])

y = [10,2,4,6,7,8]
print(y[::2])
print(y[::3])
print(y[::-2])