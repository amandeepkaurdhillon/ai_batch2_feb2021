x = ["red","green","blue","orange"]
y = [10,2,5]

# Concatenation
print(x+y)

#Iteration
print(y*3)

#Membership
print("green" in x)
print("green" not in x)
print("Green" not in x)

#Add elements in list
x.append("black")
x.remove("blue")
print("x=",x)