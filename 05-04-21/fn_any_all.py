x = [False, True, False, False]
y = [True, True, True, True]
z = [False, False, False, False]

print(any(x))
print(any(z))

print(all(x))
print(all(y))
print(all(z))


ls = [11,3,5,7,81,20,23]

rs = [i%2==0 for i in ls]
# if all(rs):
if any(rs):
    print("Yes, there is a number that is divisible by 2")
else:
    print("No, there is no number that is divisible by 2")
