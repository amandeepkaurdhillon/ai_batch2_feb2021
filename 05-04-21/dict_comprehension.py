# {1:1,2:8,3:27}
d = {}
for i in range(1,11):
    d[i]=i**3

print("d=",d)

### Use dictionary comprehension 

# cb = {n for n in [10,10,10,11]}

cb = {n:n**3 for n in range(1,11)}
print("cb=",cb)

empname = ["James","Peter Parker", "Cherry","John"]

d = {"name{}".format(i+1):empname[i] for i in range(len(empname))}
print(d)
