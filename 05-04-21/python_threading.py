import threading
import time 

def fun1(x):
    for i in range(5):
        time.sleep(1)
        print("x=",x)
        print("This is function 1")

def fun2():
    for i in range(5):
        time.sleep(1)
        print("This is function = 2")

t1 = threading.Thread(target=fun1,args=(10,))
t2 = threading.Thread(target=fun2)

t1.start()
t2.start()

t1.join()
t2.join()

print("MAIN THREAD")
print("I want to execute this after")


