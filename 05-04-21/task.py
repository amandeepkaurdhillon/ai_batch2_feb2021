emp = {"james":10000, "peter":50000, "jack":11999,"shailen":29999}

d = {}
for i in emp:
    # print(i, emp[i])
    if emp[i]>15000:
        d[i] = emp[i]

print(d)

# dic = {i:emp[i] for i in emp if emp[i]>15000}
dic = {i:emp[i] for i in emp if emp[i]>15000 if emp[i]%2==0}
print("dic=", dic)