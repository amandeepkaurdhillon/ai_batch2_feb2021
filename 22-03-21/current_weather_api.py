import requests

city_name = input("Enter City Name: ")
url = "http://api.openweathermap.org/data/2.5/weather?q={}&appid=0c0f35661ebd924cafa7ee167d6bbd8a&units=metric".format(city_name)

data = requests.get(url).json()
if data["cod"]==200:
    print("Here are wea`ther deatails of {} ({})".format(data["name"],data["sys"]["country"]))
    print("\tTemperature: {}°C".format(data["main"]["temp"]))
    print("\tHumidity: {}°C".format(data["main"]["humidity"]))
    print("\t{} ({})".format(data["weather"][0]["main"],data["weather"][0]["description"]))

    import matplotlib.pyplot as plt 
    import matplotlib.image as mimg 

    #To install matblotlib = pip install matplotlib

    img = mimg.imread("http://openweathermap.org/img/wn/{}@2x.png".format(data["weather"][0]["icon"]))
    plt.imshow(img)
    plt.title("Weather details of {}".format(city_name))
    plt.show()
else:
    print("OOPs something went wrong!")
    print(data["message"])