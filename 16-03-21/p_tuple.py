a = (10,20,30,10,20,21,0)
b = 10,20,30
print(b)

#Insert element
# b[1] = 100 ## Python tuple is a immutable type
# del b[1]
# del b
# print(type(b))

# print(dir(b))

print(b.index(10))
print(a.index(10,1))

print(a.count(0))
print(a.count(10))