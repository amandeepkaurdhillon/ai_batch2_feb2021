a = {10,20,21,23,44}
b = {10,23}

print(a.issubset(b))
print(b.issubset(a))

print(a.issuperset(b))
print(b.issuperset(a))