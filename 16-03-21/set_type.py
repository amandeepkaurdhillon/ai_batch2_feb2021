## SET IS A COLLECTION OF UNORDERED & UNIQUE ELEMENTS
x = {10,0,22,3,3,3,3,3,3}
# print(x)

a = {10,20,30}
b = {100,10,200,30}

## Symmetric Difference
print(a.symmetric_difference(b))
print(a^b)

## Union
# u = a|b
# u = a.union(b)
# print(u)

## Interation
# print(a&b)
# print(a.intersection(b))

## Difference
# print(a-b)
# print(b.difference(a))

# print(type(a))
# print(dir(b))