col = ("Red","Green","Blue","Magenta","Red","White","Red")
fruit = ("Apple","Grapes")
# print(col.index("Red",5))

## Concatenation
print(col+fruit)

## Iteration
print(fruit*3)

## Membership
print("Orange" in fruit)
print("Orange" not in fruit)