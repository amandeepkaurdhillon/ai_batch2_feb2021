# s = set() ##Empty set
ls = [21,8,921,9,1,9]
# print("ls before =",ls)
s = set(ls) ## Type casting
# print("ls after = ",list(s))
s.add(10)
s.add(101)
s.add(10)
print("Before: ",s)
s.pop()
s.pop()
s.remove(921)
print("After: ",s)


xy = {10,11,10,10,10,10}
xy.remove(10)
print(xy)