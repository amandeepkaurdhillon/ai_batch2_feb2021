# Open File
file = open("data/myfile.txt", "w")

# Write File 
# file.write("How are you?")

for i in range(1,11):
    file.write(str(2*i)+"\n")

print("File written successfully!")



# Close File
file.close()


'''
w
    File create if not exist
    otherwise overwrite file
    IN txt files, data must be in str format

'''