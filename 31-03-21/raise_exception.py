try:
    a = "hello there"
    print(a)
    raise ValueError
    print("after error")
    # raise NameError #To call exception forcefully

except NameError:
    print("Check names again!")