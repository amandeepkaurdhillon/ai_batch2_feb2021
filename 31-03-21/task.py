import requests 

data = requests.get("https://restcountries.eu/rest/v2/all").json()
d = ""
for i in data:
    d+= "<div style='height:350px;width:30%;margin:1%;"
    d+= "box-shadow:0px 0px 10px gray;float:left;text-align:center;'>"
    d += "<h4>{}</h4>".format(i["name"])
    d += "<p><em>Population: {}</em></p>".format(i["population"])
    d += "<p><em>Borders: {}</em></p>".format(i["borders"])
    d += "<img src='{}' style='height:200px;width:200px;'>".format(i["flag"])
    d += "<hr/>"
    d+= "</div>"
f = open("data/countries.html","w")
f.write(d)
f.close()

print("file written successfully!")