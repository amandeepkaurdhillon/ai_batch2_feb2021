class MyError(Exception):
    def __init__(self):
        self.d = "Variable value Error!"

    def __str__(self):
        return self.d



try:
    a = int(input("Enter Value: "))
    b = int(input("Enter Value: "))
    if a<b:
        raise MyError 
    else:
        print("Result: ", a-b)

except MyError as ab:
    print(ab)
    print("First value must be greater than second one")