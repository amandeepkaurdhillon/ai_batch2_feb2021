try:
    a = 10
    b = "Aman"
    import abcd
    # a/0
    # print(e)
    # print(a+b)

except (NameError, TypeError):
    print("Please check code & try again!")

except ZeroDivisionError:
    print("Numbers can't be divided by zero")

except:
    print("OOPS Something went wrong!!!")