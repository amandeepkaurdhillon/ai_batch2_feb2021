class Arithmetic:
    def sum(self,x,y):
        print(x+y)

    def sum(self,x,y,z):
        print(x+y+z)

obj = Arithmetic()
obj.sum(10,20,30)
# obj.sum(10,20) 
'''
    - Python doesn't support method overloading
    - If there are two functions of same name, then 
    lastest one will be used
'''


## Operator overloading
# a = "10"
# b = "20"

# print(a+b)