class Student:
    #Public data member
    clz = "ABCD College of Management"

    #Public member function
    def hello(self):
        print("Hello Everyone!")

class Child(Student):
    def intro(self):
        print("Inside: ",self.clz)
        #Call member function of parent class
        self.hello()
        print("Member function of intro class ")

ob = Child()
ob.clz = "HELLO COLLEGE"
print(ob.clz)
ob.intro()
