class Library:
    a = 10
    def __init__(self):
        print("Constructor of library class")

class Student(Library):
    a = 20
    def __init__(self):
        Library.__init__(self) #Call parent class's M.F.
        print("Constructor of Student Class")
        print(Library.a)
        super().__init__()


obj = Student()

'''
 If both parent and child class have functions of same name,
 then child's object will override parent's M.F. 
 (Child class's function will be used)
'''