class Student:
    __pin = 10001 #Private D.M.
    def __intro(self): #Private Member Function
        print("MF of parent class")

class Derived(Student):
    def intro1(self):
        # self.__intro()
        # print("protected PIN CODE: ", self.__pin) ## child can't use private data members
        print("MF of child class")

obj = Derived()
obj.intro1()
# print(obj.__pin)

