Polymorphism 
    - Method Overloading 
    - Method Overriding

    Python doesn't support method overloading

Error - It disrupts normal flow of execution and execute unrequired result

The process of handling errors is called exception handloing.

There are following types of errors: 
1. IndentationError
2. SyntaxError 
3. NameError
4. KeyError
5. IndexError
6. ZeroDivisionError
7. MemoryError
8. ValueError
9. FileNotFoundError
10. ModuleNotFoundError
11. IOError 