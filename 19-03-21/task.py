# INSTALL REQUESTS USING 'pip install requests'
# !pip install requests
import requests 
data = requests.get("https://restcountries.eu/rest/v2/all").json()
ct = input("Enter Name to Search: ")
f = False
for i in data:
    if i["name"].lower()==ct.lower():
        f = True
        print("=========== Here are details of {} ({}) =============".format(i["name"],i["region"]))
        print("\tCapital: {}".format(i["capital"]))
        print("\tPopulation: {} people".format(i["population"]))
        print("\tArea: {}".format(i["area"]))
        print("\tFlag URL: {}".format(i["flag"]))
        print("\tBorders ({})".format(len(i["borders"])))
        c =1
        for b in i["borders"]:
            print("\t\t {}. {}".format(c,b))
            c+=1
        print()
        print("\tLanguages ({})".format(len(i["languages"])))
        d =1
        for l in i["languages"]:
            print("\t\t {}. {}".format(d,l["name"]))
            d+=1

if f==False:
    print("Could not find '{}' in our database".format(ct))