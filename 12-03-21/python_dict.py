student = {
    "name":"Peter Parker",
    "roll_no":123,
    "email":"peter.parker@gmail.com",
    "subjects":["Python","PHP","AI","Cloud Computing"],
    "is_present":True,
}
# print(student)
#Access elements
print(student["name"])
print(student["subjects"][-1])

# print(student.get("email"))
## Insert element
student["branch"]="CSE"

## Update elements
student["name"] = "Harry Potter"

## Delete Element
del student["subjects"]
print(student)