student = {
    "name":"Peter Parker",
    "roll_no":123,
    "email":"peter.parker@gmail.com",
    "subjects":["Python","PHP","AI","Cloud Computing"],
    "is_present":True,
}

for i in student:
    print(i,"=",student[i])

## Membership
print("name" in student)
print("age" not in student)

a = [
    {"name":"Jack","age":19,"contact":9898989898},
    {"name":"Peter","age":20,"contact":1988787877},
    {"name":"Amandeep Kaur","age":28,"contact":9898565656},
    {"name":"Nobita","age":25,"contact":9999999999},
]

for i in a:
    if i["age"]<=20:
        print("Name:",i["name"])
        print("Age:",i["age"])
        print("------------------")