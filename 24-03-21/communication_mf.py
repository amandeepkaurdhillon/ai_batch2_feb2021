class Student:
    college = "XYZ Engg. College"

    def register(self, name, email, rno):
        self.n = name
        self.em = email
        self.r = rno
        print("{} Registered successfully!".format(name))

    def display_student(self):
        print("\nName: {}".format(self.n))
        print("Email: {}".format(self.em))
        print("Roll No: {}\n".format(self.r))

st1 = Student()
st1.register("James","james.j1@gmail.com",1001)
st1.display_student()

st2 = Student()
st2.register("Jenny Payen","jenny.j1@gmail.com",1002)
st2.display_student()

st3 = Student()
st3.register("Mr. Robert","robert@gmail.com",1003)
st3.display_student()
