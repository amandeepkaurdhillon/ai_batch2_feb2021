class Student:
    #Data members
    name = "Peter"
    age = 10
    intro = "It is a data member"

    #Member Function
    def profile(self):
        print("{} is {} years old".format(self.name, self.age))
        print("It is a member function")

st1 = Student()
print(st1.name)
print(st1.intro)
st1.profile()