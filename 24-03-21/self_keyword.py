class Student:
    clz = "XYZ Engg. College"

    def profile(self, name, age):
        print("Name: {} Age: {} College Name: {}".format(name, age, self.clz))

o1 = Student()
o1.profile("Peter Parker",5)

o2 = Student()
o2.profile("Schinchan",5)

print(o1.clz)
# o2.clz = "ABCD cOLLEGE OF Management"
# del o2.clz
print(o2.clz)