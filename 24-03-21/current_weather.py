import requests

c  = input("Enter City Name: ")

d = requests.get("http://api.openweathermap.org/data/2.5/weather?q="+c+"&appid=c70a71da13017d87057e29848f4ff889&units=metric").json()
# print(d)
print("Here are weather detail of: {} ({})".format(d["name"],d["sys"]["country"]))
print("\tTemperature: {}°C".format(d["main"]["temp"]))
print("\tHumidity: {}".format(d["main"]["humidity"]))
print("\t{} ({})".format(d["weather"][0]["main"],d["weather"][0]["description"]))
print("http://openweathermap.org/img/wn/02d@2x.png")

import matplotlib.pyplot as plt
import matplotlib.image as img

myimage = img.imread("http://openweathermap.org/img/wn/{}@2x.png".format(d["weather"][0]["icon"]))
plt.imshow(myimage)
plt.title("{} ({})".format(c,d["weather"][0]["main"] ))
plt.show()