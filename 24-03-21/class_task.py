class Circle:
    pi=3.14 
    def set_radius(self, rad):
        self.r = rad
        print("Radius set to {}".format(rad))

    def calc_area(self):
        return self.pi*self.r**2

    def calc_param(self):
        return 2*self.pi*self.r

c1 = Circle()
c1.set_radius(10)
c2 = Circle()
c2.set_radius(5)
c3 = Circle()
c3.set_radius(100)

print(c1.calc_area())
print(c1.calc_param())
print(c2.calc_param())
print(c3.calc_param())
