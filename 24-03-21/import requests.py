import requests

city_name = input("Enter City Name: ")
url = "http://api.openweathermap.org/data/2.5/forecast?q={}&appid=185e8105f692c832734e4ea3b68ba805".format(city_name)

data = requests.get(url).json() 
print("Here are weather deatails of {} ({})".format(data["city"]["name"],data["country"]))

for i in data.list:
    print("\tTemperature: {}°C".format(i["main"]["temp"]))
    print("\tHumidity: {}°C".format(i["main"]["humidity"]))
    print("\t{} ({})".format(i["weather"][0]["main"],i["weather"][0]["description"]))
